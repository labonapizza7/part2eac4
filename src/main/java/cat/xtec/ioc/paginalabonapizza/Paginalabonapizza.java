package cat.xtec.ioc.paginalabonapizza;

import java.io.*;

/**
 *
 * @author josepmaria
 */
public class Paginalabonapizza {

    public static void main(String[] args) throws IOException {
        
        File f = new File("paginalabonapizza.html");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(f))) {
            bw.write("<html>");bw.newLine();
            bw.write("  <head>");bw.newLine();
            bw.write("    <title>");bw.newLine();
            bw.write("      La Bona Pizza");bw.newLine();
            bw.write("    </title>");bw.newLine();
            bw.write("  </head>");bw.newLine();
            bw.write("  <body>");bw.newLine();
            bw.write(" <h1>Soc en/na Joan Busquet Zamora </h1>");bw.newLine();
            bw.write(" <p>He aprés molt fent el mòdul 8 de DAW!!!</p>");bw.newLine();
            bw.write("    La Bona Pizza");bw.newLine();
            bw.write("  </body>");bw.newLine();
            bw.write("</html>");bw.newLine();
            bw.close();
            } 
    }
}
